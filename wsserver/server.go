package wsserver

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	// Resolve cross-domain problems
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type WServer struct {
	Addr string
	Hub  *FortHub
}

func NewWServer(addr string) *WServer {
	wg := WServer{}

	hub := NewFortHub()

	wg.Addr = addr
	wg.Hub = hub

	return &wg
}

func (ws *WServer) Main(errc chan error) {
	//	hub := newHub()
	go ws.Hub.Run()

	//http.HandleFunc("/", serveHome)
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		ws.ServeWs(ws.Hub, w, r)
	})
	err := http.ListenAndServe(ws.Addr, nil)
	if err != nil {

		errc <- err
		//log.Fatal("ListenAndServe: ", err)
	}
}

func (ws *WServer) ServeWs(hub *FortHub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &FortClient{Hub: hub, Conn: conn, Send: make(chan []byte, 256)}
	fmt.Println("sending client onn chan")
	client.Hub.Clientregister <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.WritePump()
	go client.ReadPump()
}
