package wsserver

import (
	"encoding/json"
	"errors"
	"fmt"
	"local/ant1/fortserver/fort"

	"github.com/RichardKnop/machinery/v2"
	//"github.com/RichardKnop/machinery/v2/config"
	redisbackend "github.com/RichardKnop/machinery/v2/backends/redis"
	redisbroker "github.com/RichardKnop/machinery/v2/brokers/redis"
	machconfig "github.com/RichardKnop/machinery/v2/config"
	eagerlock "github.com/RichardKnop/machinery/v2/locks/eager"
	"github.com/RichardKnop/machinery/v2/tasks"
)

type FortHub struct {
	Clients map[*FortClient]bool

	// Inbound messages from the clients.
	//apw server reports
	Interests map[string][]*FortClient

	Clientbroadcast chan []byte

	InterestsBroadcast chan []byte

	// Register requests from the clients.
	Clientregister chan *FortClient

	// Unregister requests from clients.
	Clientunregister chan *FortClient

	InterestsRegister chan string

	InterestsUnRegister chan string
}

func NewFortHub() *FortHub {

	return &FortHub{
		Clients:             make(map[*FortClient]bool),
		Interests:           map[string][]*FortClient{},
		Clientbroadcast:     make(chan []byte),
		InterestsBroadcast:  make(chan []byte),
		InterestsRegister:   make(chan string),
		InterestsUnRegister: make(chan string),
		Clientregister:      make(chan *FortClient),
		Clientunregister:    make(chan *FortClient),
	}
}

type MsgTest struct {
	Pwserver    string `json:"pwserver"`
	MoveName    string `json:"movename"`
	Action      string `json:"action"`
	ActionValue string `json:"actionvalue"`
}

//{"pwserver":"ServerName 1","movename":"MoveName 1","action":"Action e","actionvalue":"some, text here"}

func (fh *FortHub) Run() {

	//var wg sync.WaitGroup
	for {
		select {
		case client := <-fh.Clientregister:
			fh.Clients[client] = true
		case client := <-fh.Clientunregister:
			if _, ok := fh.Clients[client]; ok {
				delete(fh.Clients, client)
				close(client.Send)
			}
		case message := <-fh.Clientbroadcast:

			fmt.Println(string(message))
			//var msg fort.ClientBTNFortMsg
			var msg MsgTest
			err := json.Unmarshal(message, &msg)

			if err != nil {
				fmt.Println("failed to unmarshall", err)
				errors.New("failed to unmarshall message")
			}
			fmt.Println(msg)

			// switch msg.Incoming {
			// case true:
			// 	wg.Add(1)
			// 	go func() {

			// 		err := SendMessage(msg)

			// 		if err != nil {
			// 			fmt.Println("some error here", err)
			// 		}
			// 		wg.Done()

			// 	}()
			// }

			// for client := range fh.Clients {
			// 	select {
			// 	case client.Send <- message:
			// 	default:
			// 		close(client.Send)
			// 		delete(fh.Clients, client)
			// 	}
			// }
		}
	}
}

func SendMessage(msg fort.ClientBTNFortMsg) error {

	server, err := MoveServerCreator(msg)

	if err != nil {

	}

	msgforamain := fort.MSGFORAMAIN{}
	msgforamain.ToMove = msg.MainMoveName
	msgforamain.Action = msg.MainActionName
	msgforamain.ActionValue = msg.ActionValue
	msgforamain.PlayNow = msg.PlayNow

	marshall, err := json.Marshal(msgforamain)

	task := tasks.Signature{
		Name: msg.MainMoveName,
		Args: []tasks.Arg{
			{Type: "[]byte",
				Value: marshall},
		},
	}

	_, err = server.SendTask(&task)

	return err
}

func MoveServerCreator(msg fort.ClientBTNFortMsg) (*machinery.Server, error) {

	to_que := msg.ForServer + msg.MainMoveName + "queue"

	confg := machconfig.Config{
		DefaultQueue: to_que,
		Redis: &machconfig.RedisConfig{
			MaxIdle:                3,
			IdleTimeout:            240,
			ReadTimeout:            15,
			WriteTimeout:           15,
			ConnectTimeout:         15,
			NormalTasksPollPeriod:  1000,
			DelayedTasksPollPeriod: 500,
		}}
	broker := redisbroker.New(&confg, fort.GlobalFort.RedisAddr, "", "", 0)
	backend := redisbackend.New(&confg, fort.GlobalFort.RedisAddr, "", "", 0)

	out_ser := machinery.NewServer(&confg, broker, backend, eagerlock.New())

	return out_ser, nil

}
