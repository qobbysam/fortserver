package main

import (
	"context"
	"fmt"
	common "local/ant1/pawnserver/common"
	"time"

	pkgs "local/ant1/fortserver/pkgs"
	utils "local/ant1/fortserver/utils"
)

type FortServer struct {
	Server common.WorkerServer
}

func NewFortServer() *FortServer {

	name := "pointerqueue"

	consumer_Tag := "FortConsumer"

	cfg := common.NewSuperConfig()

	ctx := context.WithValue(context.Background(), common.SuperConfigString, cfg)

	wg, err := common.NewWorkServer(ctx, name, consumer_Tag)

	if err != nil {

		fmt.Println("Server Creation Failed")
	}

	return &FortServer{Server: *wg}

}

func Oldmain() {

	fs_server := NewFortServer()

	TurnOnTask := utils.CreateAMoveTaskNoARGS(pkgs.TurnOn)

	TurnOffTask := utils.CreateAMoveTaskNoARGS(pkgs.TurnOf)

	server := fs_server.Server.Server

	asyncResult, err := server.SendTask(&TurnOnTask)
	if err != nil {
		fmt.Errorf("Could not send task: %s", err.Error())
	}

	results, err := asyncResult.Get(time.Millisecond * 5)

	if err != nil {
		fmt.Errorf("This not send task: %s", err.Error())
	}
	fmt.Println(results)

	asyncResult, err = server.SendTask(&TurnOffTask)
	if err != nil {
		fmt.Errorf("Could not send task: %s", err.Error())
	}

	results, err = asyncResult.Get(time.Millisecond * 5)

	if err != nil {
		fmt.Errorf("This not send task: %s", err.Error())
	}
	fmt.Println(results)

}
