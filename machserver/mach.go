package machserver

import (
	"github.com/RichardKnop/machinery/v2"
	redisbackend "github.com/RichardKnop/machinery/v2/backends/redis"
	redisbroker "github.com/RichardKnop/machinery/v2/brokers/redis"
	machconfig "github.com/RichardKnop/machinery/v2/config"
	eagerlock "github.com/RichardKnop/machinery/v2/locks/eager"
)

type LocalMachServer struct {
	Server    *machinery.Server
	Worker    *machinery.Worker
	QueueName string
}

func NewLocalMachServer() *LocalMachServer {

	wg := LocalMachServer{}

	hoststring := ""
	consumer_tag := ""

	cfg := machconfig.Config{

		Broker: "redis",
		Redis: &machconfig.RedisConfig{

			MaxIdle:                3,
			IdleTimeout:            240,
			ReadTimeout:            15,
			WriteTimeout:           15,
			ConnectTimeout:         15,
			NormalTasksPollPeriod:  1000,
			DelayedTasksPollPeriod: 500,
		},
	}

	broker := redisbroker.New(&cfg, hoststring, "", "", 0)

	//	broker := redisbroker.New(new_server_config, superconfig.RedisConfig.SocketPath, superconfig.RedisConfig.Host, superconfig.RedisConfig.Password, superconfig.RedisConfig.db)

	backend := redisbackend.New(&cfg, hoststring, "", "", 0)
	lock := eagerlock.New()
	server := machinery.NewServer(&cfg, broker, backend, lock)

	worker := server.NewWorker(consumer_tag, 1)

	wg.Server = server
	wg.Worker = worker

	wg.QueueName = worker.ConsumerTag

	return &wg

}

func (lms *LocalMachServer) AsyncStart(errc chan error) {

	err := lms.Worker.Launch()

	if err != nil {

		errc <- err
	}
}

func (lms *LocalMachServer) RegisterTasks(maptasks map[string]interface{}) error {

	//validate interface

	err := lms.Server.RegisterTasks(maptasks)

	return err

}
