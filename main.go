package main

import (
	"fmt"
	wsserver "local/ant1/fortserver/wsserver"
	"log"
	"sync"
)

func main() {

	errc := make(chan error)

	wserver := wsserver.NewWServer(":7000")

	//add := flag.String()

	defer close(errc)

	var wg sync.WaitGroup

	//machserver := machserver.NewLocalMachServer()

	fmt.Println("all has started")
	wg.Add(1)
	go wserver.Main(errc)

	//wg.Add(1)
	//go machserver.AsyncStart(errc)

	go func() {
		for {
			select {
			case err := <-errc:
				fmt.Println("err here in channel")
				log.Fatal("ListenAndServe: ", err)
				//wg.Done()
				wg.Done()

			default:
				//fmt.Println("Default called")
				//wg.Done()
				//wg.Done()
			}
		}
	}()

	wg.Wait()

}
