package fort

import "sync"

type Fort struct {
	mu sync.Mutex

	Pservers  []string
	RedisAddr string
}

func (f *Fort) AddPserver(name string) error {

	f.mu.Lock()

	defer f.mu.Unlock()

	f.Pservers = append(f.Pservers, name)

	return nil
}
