package fort

type ClientBTNFortMsg struct {
	Incoming       bool
	PlayNow        bool
	ForServer      string
	MainMoveName   string
	MainActionName string
	ActionValue    []string
}

type MSGFORAMAIN struct {
	PlayNow     bool
	ToMove      string
	Action      string
	ActionValue []string
}
